import Vue from "vue";
import Vuex from "vuex";

import { getList } from "~/assets/services/api";

Vue.use(Vuex);

const store = () =>
  new Vuex.Store({
    state: {
      rightList: [] // 右侧列表数据
    },

    mutations: {
      SET_RIGHTLIST: (state, rightList) => {
        state.rightList = rightList;
      }
    },

    actions: {
      // 获取页面右侧列表数据
      getRightList({ commit, state }) {
        return new Promise((resolve, reject) => {
          if (state.rightList.length > 0) {
            resolve(state.rightList);
          } else {
            const nd = {
              pageSize: 5,
              page: 1,
              status: 1,
              columnId: 1
            };
            getList(nd)
              .then(response => {
                commit("SET_RIGHTLIST", response.records);
                resolve(response.records);
              })
              .catch(error => {
                reject(error);
              });
          }
        });
      }
    }
  });

export default store;
