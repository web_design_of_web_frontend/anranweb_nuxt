import request from "../utils/request";
const qs = require("qs");

export const getList = d => {
  return request({
    url: `/sys/article/list`,
    method: "GET",
    params: d
  });
};

export const getInfo = id => {
  return request({
    url: `/sys/article/${id}`,
    method: "GET",
    params: ''
  });
};

// 查询置顶文章详情
export const getIsTopInfo = () => {
  return request({
    url: `/sys/article/top`,
    method: "GET",
    params: ''
  });
};

// 新增留言
export const addMsg = d => {
  return request({
    url: `/sys/msg/add`,
    method: "POST",
    data: qs.stringify(d)
  });
};

// 查询留言
export function getMsg(d) {
  return request({
    url: `/sys/msg/list`,
    method: "GET",
    params: d
  });
}

// 生成二维码
export const creatQrCode = d => {
  return request({
    url: `/an/core`,
    method: "POST",
    data: qs.stringify(d)
  });
};
