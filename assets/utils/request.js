import axios from 'axios';


// 创建axios实例
const service = axios.create({
  baseURL: "/ECS",
});
// request拦截器
service.interceptors.request.use(config => {
  return config;
}, error => {
  // Do something with request error
  console.log(error); // for debug
  Promise.reject(error);
});

// respone拦截器
service.interceptors.response.use(
  response => {
    /**
    * code为非1是抛错
    */
    const res = response.data;
    if (res.code !== 1) {
      return Promise.reject('error');
    } else {
      return res.result;
    }
  },
  error => {
    console.log('错误：' + error);// for debug
    return Promise.reject(error);
  }

);


export default service;
