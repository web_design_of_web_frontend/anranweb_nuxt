export default {
  mode: "universal",
  env: {
    baseUrl: process.env.BASE_URL || 'http://anranweb.cn'
  },
  /*
   ** Headers of the page
   */
  head: {
    title: "安然网",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "keywords",
        name: "keywords",
        content: "网页设计，网站开发，小程序，APP制作"
      },
      {
        hid: "description",
        name: "description",
        content:
          "一名专业的IT从业者，不但要会网页制作，还要会制作小程序，APP等等"
      }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },

  /*
   ** Global CSS
   */
  css: ["assets/css/anStyle.css"],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org/docs/
    "bootstrap-vue/nuxt",
    "@nuxtjs/axios",
    "@nuxtjs/proxy"
  ],
  axios: {
    proxy: true
  },
  proxy: {
    "/ECS": {
      target: "https://anranweb.cn",
      changeOrigin: true,
    }
  },

  server: {
    port: 8086, // default: 3000
    host: '0.0.0.0' // default: localhost
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });
      }
    }
  },

  generate: {
    /* routes: function () {
      let pageSize = 5;
      let nd = {
        pagesize:pageSize,
        page:1,
      };
      const qs = require('qs');
      return axios.post(`http://www.anranweb.cn/an_ranweb.php`,qs.stringify({
        q: 'articleList',
        data: nd
      })).then((res) => {
        let tol = res.data.data.total;
        let pageNum = Math.ceil(tol/pageSize);
        let newArray = [];
        for(let i = 1; i<=pageNum; i++){
          newArray.push({
            route:'/view/listPage/'+i,
            payload: i,
          })
        }
        return newArray
      })
    },
    router:{
      base:"/new/" //此为根目录，如果有具体目录需求按实际情况写
    } */
  }
};
